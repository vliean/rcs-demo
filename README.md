# rcs-sdk

#### Module说明
1. apiclient.py  接口调用模块
2. auth.py  权限模块
3. conf.py 配置模块
4. param.py 参数模块
5. response.py 响应模块
6. util.py 工具模块

#### apiclient说明
##### 提供各种业务功能方法调用的封装
1. MediaUploadClient 素材上传
2. MenuUpdateClient chatbot 菜单更新
3. ProtocolOutBoundClient 协议消息下发
4. SceneNodeSendClient  场景节点下发
5. SceneSendClient 场景下发
6. TemplateMsgCreateClient  模板消息创建
7. TemplateMsgSendClient 模板消息发送


