"""
  模块描述：服务配置
  @author 8526
  @date 2022-04-28 9:15:33
  版权所有 Copyright www.dahantc.com
"""
# 基本接口调用地址
Base_Url = 'http://rcscallback-qa.dahancloud.com'
UPLOAD_MEDIA = "/rcs/medias/upload/"  # "媒体文件上传"
MESSAGE_TEMPLATE_CREATE = "/rcs/template/addTemplate/"  # "消息模板创建"
SEND_BY_TEMPLATE = "/rcs/template/send/"  # "模板发送"
SEND_BY_PROTOCOL = "/rcs/protocol/outbound/"  # "协议发送"
SEND_BY_SCENE = "/rcs/scene/send/"  # "场景下发"
SEND_BY_SCENENODE = "/rcs/scene/node/"  # "场景节点下发"
DOWNLOAD_FILE = "/rcs/medias/download/"  # "文件下载"
MENU_UPDATE = "/chatbot/config/menu/"  # "菜单配置"
