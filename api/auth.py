"""
  模块类述：授权实体类
  @author 8526
  @date 2022-04-28 9:15:33
  版权所有 Copyright www.dahantc.com
"""
class AuthInfo:
    def __init__(self, account, pwd, chatbotid):
        self.account, self.pwd, self.chatbotId = account, pwd, chatbotid
